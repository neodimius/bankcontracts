<?php

/**
 * Class ContractController
 */
class ContractController extends Controller
{
    public function actionIndex()
    {
        $contracts = array();

        $contractsXml = simplexml_load_string(file_get_contents(__DIR__ . '/../data/account_list.xml'));

        foreach ($contractsXml as $contractXml) {
            if (isset($contractXml->id))
                $contracts[] = new Contract($contractXml);
        }

        $this->render('index', [
            'contracts' => $contracts,
        ]);
    }
}