<?php
/* @var $this ContractController */

$this->pageTitle=Yii::app()->name;
?>

<table class="table table-striped">
    <thead>
    <tr>
        <th colspan="4"></th>
        <th scope="col" class="btn-group">
            <i class="far fa-eye btn permission-all" data-permission='A'></i>
            <i class="material-icons btn permission-all" data-permission='B'>view_list</i>
            <i class="far fa-eye-slash btn permission-all" data-permission='C'></i>
            <i class="far fa-stop-circle btn permission-all" data-permission='D'></i>
        </th>
    </tr>
    <tr>
        <th scope="col">ID</th>
        <th scope="col">Contract type</th>
        <th scope="col">Product code</th>
        <th scope="col">Account number</th>
        <th scope="col">Availability</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($contracts as $contract): ?>
        <tr>
            <td scope="row">
                <span><?= $contract->numberInternal ?></span>
                <br>
                <br>
                <?= $contract->id ?>
            </td>
            <td><?= $contract->contractType ?></td>
            <td><?= $contract->type ?></td>
            <td>
                <span><?= $contract->numberInternal ?></span>
                <br>
                <br>
                <span class="bg-success active-btn"><?= $contract->status === 'active' ? 'active ' : ''; ?></span>
            </td>
            <td class="permission btn-group bg-secondary">
                <i class="far fa-eye btn <?= $contract->permission === 'A' ? 'active' : '' ?>" data-permission='A'></i>
                <i class="material-icons btn <?= $contract->permission === 'B' ? 'active' : '' ?>"
                   data-permission='B'>view_list</i>
                <i class="far fa-eye-slash btn <?= $contract->permission === 'C' ? 'active bg-warning' : '' ?>"
                   data-permission='C'></i>
                <i class="far fa-stop-circle btn <?= $contract->permission === 'D' ? 'active bg-danger' : '' ?>"
                   data-permission='D'></i>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>