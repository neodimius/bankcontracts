<?php

/**
 * Class Contract
 */
class Contract
{
    public $id;
    public $type;
    public $contractType;
    public $status;
    public $contractID;
    public $numberInternal;
    public $numberPan;
    public $currency;
    public $openDate;
    public $closeDate;
    public $permission;

    /**
     * Contract constructor.
     * @param $xml
     */
    public function __construct($xml)
    {
        $this->id = (int) $xml->id;
        $this->type = (string) $xml->type;
        $this->contractType = (string) $xml->contractType;
        $this->status = (string) $xml->status;
        $this->contractID = (string) $xml->contractID;
        $this->numberInternal = (string) $xml->numberInternal;
        $this->numberPan = (string) $xml->numberPan;
        $this->currency = (string) $xml->currency;
        $this->openDate = (string) $xml->openDate;
        $this->closeDate = (string) $xml->closeDate;
        $this->permission = (string) $xml->permission;
    }
}