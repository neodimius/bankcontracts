$(document).on('click', '.btn-group i', function () {
    var current = $(this).data('permission');

    $(this).toggleClass('active');

    // Another background colors for C and D buttons
    if (current === 'C')
        $(this).toggleClass('bg-warning');
    else if (current === 'D')
        $(this).toggleClass('bg-danger');

    // Remove active class from siblings
    $(this).siblings()
        .removeClass('bg-danger')
        .removeClass('bg-warning')
        .removeClass('active');

    // Check if the Select/Deselect All button
    if ($(this).hasClass('permission-all')) {
        var allItems = $(`[data-permission='${current}']`);

        if ($(this).hasClass('active')) {
            $('.permission i').removeClass('bg-danger')
                .removeClass('bg-warning')
                .removeClass('active');

            allItems.addClass('active');

            if (current === 'C')
                allItems.addClass('bg-warning');
            else if (current === 'D')
                allItems.addClass('bg-danger');
        } else {
            allItems.removeClass('bg-danger')
                .removeClass('bg-warning')
                .removeClass('active');
        }
    } else {
        // Check Select/Deselect All button active and if it is possible
        if ($('.permission-all.active') !== 'undefined') {
            var activeSelectBtn = $('.permission-all.active');
            var countItems = $(`[data-permission='${ activeSelectBtn.data(('permission')) }']`).length;

            if ($(`.active [data-permission='${ activeSelectBtn.data('permission') }']`).length !== countItems) {
                activeSelectBtn.removeClass('bg-danger')
                    .removeClass('bg-warning')
                    .removeClass('active');
            }
        }

    }
})
